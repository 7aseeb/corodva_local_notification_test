import {Component} from '@angular/core';
import {Platform} from "ionic-angular";

import {LocalNotifications} from '@ionic-native/local-notifications';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(private localNotifications: LocalNotifications,
              platform: Platform) {
    platform.ready().then(() => {
      // Add a "trigger" callback
      this.localNotifications.on(
        "trigger",
        (notification, state) => {
          console.debug('trigger callback!');
        }
      );
        // Schedule a single notification
        let notificationTime = new Date("September 19, 2017 23:43:00");
        this.localNotifications.schedule({
          id: 1,
          at: notificationTime.getTime(),
          text: 'Test notification',
          sound: 'file://assets/sound/beep.mp3'
        });

      }
    );
  }

}
